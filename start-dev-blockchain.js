'use strict';

const ganache = require('ganache-cli');
const path = require('path');
const fs = require('fs');

const config = require('./config').networks.dev;

const BLOCKCHAIN_DB_PATH = path.relative('.', 'blockchain-data');
const MNEMONIC = 'pigeon uncle please wolf distance dwarf comfort burden survey size leisure punch';

const server = ganache.server({
  port: config.port,
  mnemonic: MNEMONIC,
  logger: console,
  'network_id': config.networkID,
  'db_path': BLOCKCHAIN_DB_PATH,
});

const newDB = !fs.existsSync(BLOCKCHAIN_DB_PATH);

if (newDB) {
  fs.mkdirSync(BLOCKCHAIN_DB_PATH);
}

server.listen(config.port, (error, blockchain) => {
  if (error) {
    console.error(error);
    throw error;
  } else {
    console.log('Dev blockchain is running');
  }
});
'use strict';

const HDWalletProvider = require('truffle-hdwallet-provider');

const config = require('./config');

const MNEMONIC = process.env.MNEMONIC;
const {dev} = config.networks;

module.exports = {
  networks: {
    development: dev,
    staging: {
      provider: () => {
        return new HDWalletProvider(MNEMONIC, 'https://ropsten.infura.io/');
      },
      network_id: 3,
    },
  },
};